FROM golang:1.12

WORKDIR /go/src/gitlab.com/tokend/accountManager

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o /usr/local/bin/accountManager gitlab.com/tokend/accountManager


###

FROM alpine:3.9

COPY --from=0 /usr/local/bin/accountManager /usr/local/bin/accountManager
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["accountManager"]
