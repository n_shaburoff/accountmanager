package horizon

import (
	"gitlab.com/distributed_lab/json-api-connector/client"
	"gitlab.com/tokend/connectors/keyvalue"
	"gitlab.com/tokend/horizon-connector"
)

type Connector struct {
	*horizon.Connector
	keyValuer *keyvalue.KeyValuer
}

func NewConnector(connector *horizon.Connector, client client.Client) *Connector {
	valuer := keyvalue.New(client)
	return &Connector{
		Connector: connector,
		keyValuer: valuer,
	}
}


