package accountcreator

import (
	"context"
	"encoding/json"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/accountManager/internal/connectors/horizon"
	"gitlab.com/tokend/accountManager/internal/data"
	"gitlab.com/tokend/go/xdrbuild"
)

type AccountCreator struct {
	Tx             *xdrbuild.Transaction
	Horizon        *horizon.Connector
	SystemSettings data.SystemSettings
}

type AccountDetails struct {
	AccountID string
	Role      *uint64
	Signers   []data.AccountSigner
}

func New(tx *xdrbuild.Transaction,
	horizon *horizon.Connector, systemSettings data.SystemSettings) AccountCreator {
	return AccountCreator{
		Tx:             tx,
		Horizon:        horizon,
		SystemSettings: systemSettings,
	}
}

func (c AccountCreator) CreateAccount(ctx context.Context, details AccountDetails) error {
	tx := c.Tx

	defaultAccountRole, err := c.SystemSettings.DefaultAccountRole()
	if err != nil {
		return errors.Wrap(err, "failed to get account role")
	}

	role := defaultAccountRole
	if details.Role != nil {
		role = *details.Role
	}

	tx = c.account(tx, details.AccountID, details.Signers, role)

	envelope, err := tx.Marshal()
	if err != nil {
		return errors.Wrap(err, "failed to build tx envelope")
	}

	result := c.Horizon.Submitter().Submit(ctx, envelope)
	if result.Err != nil {
		return errors.Wrap(err, "failed answer from horizon")
	}
	return nil
}

func (c AccountCreator) account(tx *xdrbuild.Transaction, accountID string, accountSigners []data.AccountSigner, accountRole uint64) *xdrbuild.Transaction {
	tx = tx.Op(&xdrbuild.CreateAccount{
		Destination: accountID,
		RoleID:      accountRole,
		Signers:     c.getSigners(accountSigners),
	})
	return tx
}

func (c AccountCreator) getSigners(accountSigners []data.AccountSigner) []xdrbuild.SignerData {
	var signers []xdrbuild.SignerData
	for _, accountSigner := range accountSigners {
		signers = append(signers, xdrbuild.SignerData{
			PublicKey: accountSigner.SignerID,
			RoleID:    accountSigner.RoleID,
			Weight:    accountSigner.Weight,
			Identity:  accountSigner.Identity,
			Details:   Details{},
		})
	}
	return signers
}

type Details map[string]interface{}

func (d Details) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]interface{}(d))
}



