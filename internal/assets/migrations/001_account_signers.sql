-- +migrate Up

CREATE TABLE accounts (
                account_id character(52) NOT NULL,
                account_role bigint NOT NULL
);

CREATE TABLE account_signers (
                account_id character(52) not null REFERENCES accounts(account_id) ON UPDATE CASCADE,
                signer_id text not null ,
                role_id   integer not null,
                weight    integer not null,
                identity  integer not null
);

-- +migrate Down

DROP TABLE accounts cascade ;
DROP TABLE account_signers cascade ;


