package config

import (
    "gitlab.com/distributed_lab/kit/comfig"
    "gitlab.com/distributed_lab/kit/copus"
    "gitlab.com/distributed_lab/kit/copus/types"
    "gitlab.com/distributed_lab/kit/kv"
    "gitlab.com/distributed_lab/kit/pgdb"
    "gitlab.com/tokend/connectors/signed"
    horizon "gitlab.com/tokend/horizon-connector"
)

type Config interface {
    comfig.Logger
    pgdb.Databaser
    types.Copuser
    comfig.Listenerer
    signed.Clienter
    Horizon() *horizon.Connector
}

type config struct {
    comfig.Logger
    pgdb.Databaser
    types.Copuser
    comfig.Listenerer
    signed.Clienter
    *horizon.Horizoner
    getter kv.Getter
}

func New(getter kv.Getter) Config {
    return &config{
        getter:     getter,
        Databaser:  pgdb.NewDatabaser(getter),
        Copuser:    copus.NewCopuser(getter),
        Listenerer: comfig.NewListenerer(getter),
        Logger:     comfig.NewLogger(getter, comfig.LoggerOpts{}),
        Horizoner:  horizon.NewHorizoner(getter),
        Clienter:   signed.NewClienter(getter),
}
}
