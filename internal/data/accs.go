package data

type Accs interface {
	New() Accs
	Create(acc Acc) (Acc, error)
	CreateSigners(signers []AccountSigner) ([]AccountSigner, error)
	Select() ([]Acc, error)
	Transaction(fn func(q Accs) error) error
}

type Acc struct {
	AccountID string `db:"account_id"`
	AccRole uint64 `db:"account_role"`
}
