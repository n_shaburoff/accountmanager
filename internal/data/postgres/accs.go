package postgres

import (
	"database/sql"
	sq "github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/accountManager/internal/data"
)

type Accs struct {
	db  *pgdb.DB
	sql sq.SelectBuilder
}


func (q *Accs) CreateSigners(signers []data.AccountSigner) ([]data.AccountSigner, error) {
	if len(signers) == 0 {
		return nil, errors.New("empty array is not allowed")
	}

	names := []string{
		"account_id",
		"role_id",
		"weight",
		"identity",
	}
	stmt := sq.Insert("account_signers").Columns(names...)
	for _, item := range signers {
		stmt = stmt.Values([]interface{}{
			item.AccountID,
			item.RoleID,
			item.Weight,
			item.Identity,
		}...)
	}

	stmt = stmt.Suffix("returning *")
	var result []data.AccountSigner
	err := q.db.Select(&result, stmt)

	return result, err

}

func (q *Accs) New() data.Accs {
	return NewAccs(q.db)
}

func (q *Accs) Create(acc data.Acc) (data.Acc, error) {
	stmt := sq.Insert("account_signers").SetMap(map[string]interface{}{
		"account_id": acc.AccountID,
		"account_role": acc.AccRole,
	})
	var result data.Acc
	err := q.db.Get(&result, stmt)
	return result, err
}

func (q *Accs) Select() ([]data.Acc, error) {
	var result []data.Acc
	err := q.db.Select(&result, q.sql)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
	}
	return result, err
}

var accsSelect = sq.
	Select("a.*").
	From("accounts")

func NewAccs(db *pgdb.DB) data.Accs {
	return &Accs{
		db: db.Clone(),
		sql:  accsSelect,
	}
}

func (q *Accs) Transaction(fn func(q data.Accs) error) error {
	return q.db.Transaction(func() error {
		return fn(q)
	})
}