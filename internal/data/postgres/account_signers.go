package postgres

import (
	"database/sql"
	sq "github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/tokend/accountManager/internal/data"
)

type AccountSigners struct {
	db  *pgdb.DB
	sql sq.SelectBuilder
}

func (q *AccountSigners) Transaction(fn func(q data.AccountSigners) error) error {
	return q.db.Transaction(func() error {
		return fn(q)
	})

}

var accountSignersSelect = sq.
	Select("a.*").
	From("account_signers a")

func NewAccountSigners(db *pgdb.DB) *AccountSigners {
	return &AccountSigners{
		db: db.Clone(),
		sql:  accountSignersSelect,
	}
}

func (q *AccountSigners) New() data.AccountSigners {
	return NewAccountSigners(q.db)
}

func (q *AccountSigners) Create(signer data.AccountSigner) error {
	stmt := sq.Insert("account_signers").SetMap(map[string]interface{}{
		"account_id": signer.AccountID,
		"signer_id": signer.SignerID,
		"role_id":   signer.RoleID,
		"identity":  signer.Identity,
		"weight":    signer.Weight,
	})

	var result data.AccountSigner
	err := q.db.Get(&result, stmt)
	return err
}

func (q *AccountSigners) Select() ([]data.AccountSigner, error) {
	var result []data.AccountSigner
	err := q.db.Select(&result, q.sql)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
	}
	return result, err

}









