package data

type AccountSigners interface {
	New() AccountSigners
	Create(signer AccountSigner) error
	Select() ([]AccountSigner, error)
	Transaction(fn func(q AccountSigners) error) error
}

type AccountSigner struct {
	AccountID string `db:"account_id"`
	SignerID string `db:"signer_id"`
	RoleID   uint64 `db:"role_id"`
	Weight   uint32 `db:"weight"`
	Identity uint32 `db:"identity"`
}



