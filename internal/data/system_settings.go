package data

import (
	"encoding/json"
	"fmt"

	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/go/xdr"
	"gitlab.com/tokend/horizon-connector"
	regources "gitlab.com/tokend/regources/generated"
)

type KeyValue struct {
	DefaultAccountRole     string `fig:"account_role"`
}

type SystemSettings interface {

	DefaultAccountRole() (uint64, error)

}

type SystemSettingsQ struct {
	Horizon  *horizon.Connector
	KeyValue KeyValue

	// Cached value of KycRecoverySignerRole
	kycRecoverySignerRole *uint64
}

func NewSystemSettingsQ(horizon *horizon.Connector, kv KeyValue) SystemSettings {
	return &SystemSettingsQ{
		Horizon:  horizon,
		KeyValue: kv,
	}
}

func (q *SystemSettingsQ) DefaultAccountRole() (uint64, error) {
	accountRole, err := keyValueUint32(q.Horizon, q.KeyValue.DefaultAccountRole)
	if err != nil {
		return 0, errors.Wrap(err, "failed to get role")
	}

	if accountRole == nil {
		return 0, errors.New("role id does not exist")
	}
	return uint64(*accountRole), nil
}

func keyValueUint32(h *horizon.Connector, key string) (*uint32, error) {
	resp, err := h.Client().Get(fmt.Sprintf("/v3/key_values/%s", key))
	if err != nil {
		return nil, errors.Wrap(err, "failed to get key value")
	}

	if resp == nil {
		return nil, nil
	}

	var result regources.KeyValueEntryResponse
	if err := json.Unmarshal(resp, &result); err != nil {
		return nil, errors.Wrap(err, "Failed to unmarshal key value")
	}
	if t := result.Data.Attributes.Value.Type; t != xdr.KeyValueEntryTypeUint32 {
		return nil, fmt.Errorf("invalid key value type: %v", t)
	}

	return result.Data.Attributes.Value.U32, nil
}


