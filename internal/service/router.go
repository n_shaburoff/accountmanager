package service

import (
    "github.com/go-chi/chi"
    "gitlab.com/distributed_lab/ape"
    "gitlab.com/tokend/accountManager/internal/config"
    "gitlab.com/tokend/accountManager/internal/connectors/horizon"
    "gitlab.com/tokend/accountManager/internal/data/postgres"
    "gitlab.com/tokend/accountManager/internal/service/handlers"
)

func (s *service) router(cfg config.Config) chi.Router {
    r := chi.NewRouter()
    horizonConnector := horizon.NewConnector(cfg.Horizon(), cfg.Client())

    r.Use(
        ape.RecoverMiddleware(s.log),
        ape.LoganMiddleware(s.log), // this line may cause compilation error but in general case `dep ensure -v` will fix it
        ape.CtxMiddleware(
            handlers.CtxLog(s.log),
            handlers.CtxAcc(postgres.NewAccs(cfg.DB())),
            handlers.CtxAccountSignersQ(postgres.NewAccountSigners(cfg.DB())),
            handlers.CtxHorizon(horizonConnector),
        ),
    )

    // configure endpoints here
    r.Route("/accounts", func(r chi.Router) {
        r.Post("/", handlers.CreateAccount)
        r.Get("/", handlers.GetAccountsList)
    })
        return r
}
