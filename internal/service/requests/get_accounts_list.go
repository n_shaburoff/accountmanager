package requests

import (
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/urlval"
	"net/http"
)

type AccountListRequest struct {
	pgdb.OffsetPageParams
	IncludeSigners      bool       `include:"signers"`
}

func NewGetAccountsListRequest(r *http.Request) (AccountListRequest, error) {
	request := AccountListRequest{}

	err := urlval.Decode(r.URL.Query(), &request)
	if err != nil {
		return request, err
	}

	return request, nil
}



