package service

import (
    "gitlab.com/tokend/horizon-connector"
    "net"
    "net/http"

    "gitlab.com/distributed_lab/kit/copus/types"
    "gitlab.com/distributed_lab/logan/v3"
    "gitlab.com/distributed_lab/logan/v3/errors"
    "gitlab.com/tokend/accountManager/internal/config"
)

type service struct {
    log      *logan.Entry
    copus    types.Copus
    listener net.Listener
    connector *horizon.Connector
}

func (s *service) run(cfg config.Config) error {
    // TODO implement custom logic here
    r := s.router(cfg)
    if err := s.copus.RegisterChi(r); err != nil {
        return errors.Wrap(err, "cop failed")
    }


    return http.Serve(s.listener, r)
    return nil
}

func newService(cfg config.Config) *service {
    return &service{
        log:        cfg.Log(),
        copus:      cfg.Copus(),
        listener:   cfg.Listener(),
        connector:  cfg.Horizon(),
    }
}

func Run(cfg config.Config) {
    if err := newService(cfg).run(cfg); err != nil {
        panic(err)
    }
}
