package handlers

import (
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/tokend/accountManager/internal/data"
	"gitlab.com/tokend/accountManager/internal/service/requests"
	"gitlab.com/tokend/accountManager/resources"
	"net/http"
)

func GetAccountsList(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewGetAccountsListRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	accsQ := AccQ(r)
	accs, err := accsQ.Select()
	if err != nil {
		Log(r).WithError(err).Error("failed to get accounts")
		ape.Render(w, problems.InternalError())
		return
	}

	signersQ := AccountSignersQ(r)
	signers, err := signersQ.Select()

	if err != nil {
		Log(r).WithError(err).Error("failed to get signers")
		ape.Render(w, problems.InternalError())
		return
	}

	result := resources.CreateAccountListResponse{
		Data: newAccountsList(accs, signers),
	}
	if request.IncludeSigners {
		result.Included = newAccountIncluded(signers)
	}
	ape.Render(w, result)
}

func newAccountsList(accounts []data.Acc, signers []data.AccountSigner) []resources.CreateAccount {
	result := make([]resources.CreateAccount, len(accounts))
	for i, account := range accounts {
		accountSigners := make([]data.AccountSigner, 0, len(signers))
		for _, signer := range signers {
			if signer.AccountID == account.AccountID {
				accountSigners = append(accountSigners, signer)
			}
		}
		result[i] = newAccModel(account, accountSigners)
	}
	return result
}

func newAccountIncluded(signers []data.AccountSigner) resources.Included {
	result := resources.Included{}
	for _, item := range signers {
		resource := newSignerModel(item)
		result.Add(&resource)
	}
	return result
}


func newSignerModel(signer data.AccountSigner) resources.Signer {
	return resources.Signer{
		Key: resources.Key{
			ID: signer.SignerID,
			Type: resources.SIGNER,
		},
		Attributes: resources.SignerAttributes{
			Identity:     int32(signer.Identity),
			RoleId: 	  int32(signer.RoleID),
			Weight:       int32(signer.Weight),

		},
	}
}



