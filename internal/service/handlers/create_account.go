package handlers

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/accountManager/internal/accountcreator"
	"gitlab.com/tokend/accountManager/internal/data"
	"gitlab.com/tokend/accountManager/internal/service/requests"
	"gitlab.com/tokend/accountManager/resources"
	"net/http"
)

func CreateAccount(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewCreateAccountRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	account, err := AccountQ(r).Account(request.Data.ID)
	if err != nil {
		Log(r).WithError(err).Error("failed to get account")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	if account != nil {
		ape.RenderErr(w, problems.Conflict())
		return
	}

	signers, err := getSigners(request)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	var resultAccount data.Acc
	var resultSigners []data.AccountSigner


	err = AccQ(r).Transaction(func(q data.Accs) error{
		acc := data.Acc{
			AccRole: uint64(request.Data.Attributes.Role),
		}
		resultAccount, err = q.Create(acc)
		if err != nil {
			return errors.Wrap(err, "failed to insert account")
		}

		signers := make([]data.AccountSigner, len(request.Data.Relationships.Signers.Data))
		for i, signerKey := range request.Data.Relationships.Signers.Data {
			signer := request.Included.MustSigner(signerKey)
			signers[i] = data.AccountSigner{
				AccountID: resultAccount.AccountID,
				SignerID: signerKey.ID,
				RoleID:   uint64(signer.Attributes.RoleId),
				Weight:   uint32(signer.Attributes.Weight),
				Identity: uint32(signer.Attributes.Identity),
			}
		}
		resultSigners, err = q.CreateSigners(signers)
		if err != nil {
			return errors.Wrap(err, "failed to insert signer")
		}

		return nil
	})


	err = AccountCreator(r).CreateAccount(r.Context(), accountcreator.AccountDetails{
		AccountID: request.Data.ID,
		Signers:   signers,
	})

	result := resources.CreateAccountResponse{
		Data: newAccModel(resultAccount, resultSigners),
	}

	if err != nil {
		Log(r).WithError(err).Error("failed to create account")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	w.WriteHeader(http.StatusCreated)
	ape.Render(w, result)
}

func getSigners(request resources.CreateAccountResponse) ([]data.AccountSigner, error) {
	var signers []data.AccountSigner
	for _, signerKey := range request.Data.Relationships.Signers.Data {
		signer := request.Included.MustSigner(signerKey)
		if signer == nil {
			return nil, validation.Errors{"/included": errors.New("missed signer include")}
		}
		signers = append(signers, data.AccountSigner{
			SignerID: signerKey.ID,
			RoleID:   uint64(signer.Attributes.RoleId),
			Weight:   uint32(signer.Attributes.Weight),
			Identity: uint32(signer.Attributes.Identity),
		})
	}
	return signers, nil
}

func newAccModel(acc data.Acc, signers []data.AccountSigner) resources.CreateAccount {
	result := resources.CreateAccount{
		Key: resources.Key{
			ID:   acc.AccountID,
			Type: resources.IDENTITY_ACCOUNT,
		},
		Attributes: resources.CreateAccountAttributes{
			Role:   int64(acc.AccRole),
		},
		Relationships: resources.CreateAccountRelationships{
			Signers: resources.RelationCollection{
				Data: make([]resources.Key, len(signers)),
			},
		},
	}

	for i, item := range signers {
		result.Relationships.Signers.Data[i] = resources.Key{
			ID:   item.SignerID,
			Type: resources.SIGNER,
		}
	}

	return result
}
