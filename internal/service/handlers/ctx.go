package handlers

import (
    "context"
    "gitlab.com/distributed_lab/logan/v3"
    "gitlab.com/tokend/accountManager/internal/accountcreator"
    "gitlab.com/tokend/accountManager/internal/connectors/horizon"
    "gitlab.com/tokend/accountManager/internal/data"
    horizon2 "gitlab.com/tokend/accountManager/internal/data/horizon"
    "gitlab.com/tokend/keypair"
    "gitlab.com/tokend/regources"
    "net/http"
)

type ctxKey int

const (
    logCtxKey ctxKey = iota
    accCtxKey
    coreInfoCtxKey
    txBuilderCtxKey
    doormanCtxKey
    horizonCtxKey
    accountQCtxKey
    systemSettingsCtxKey
    accountSignersCtxKey
)

func AccountCreator(r *http.Request) accountcreator.AccountCreator {
    txbuilderbuilder := r.Context().Value(txBuilderCtxKey).(data.Infobuilder)
    info := r.Context().Value(coreInfoCtxKey).(data.Info)
    master := keypair.MustParseAddress(CoreInfo(r).MasterAccountID)
    tx := txbuilderbuilder(info, master)

    return accountcreator.New(
        tx,
        Horizon(r),
        SystemSettings(r),
    )
}

func SystemSettings(r *http.Request) data.SystemSettings {
    return r.Context().Value(systemSettingsCtxKey).(data.SystemSettings)
}

func CtxLog(entry *logan.Entry) func(context.Context) context.Context {
    return func(ctx context.Context) context.Context {
        return context.WithValue(ctx, logCtxKey, entry)
    }
}

func Log(r *http.Request) *logan.Entry {
    return r.Context().Value(logCtxKey).(*logan.Entry)
}

func CtxCoreInfo(s data.Info) func(context.Context) context.Context {
    return func(ctx context.Context) context.Context {
        return context.WithValue(ctx, coreInfoCtxKey, s)
    }
}

func CoreInfo(r *http.Request) *regources.Info {
    info, err := r.Context().Value(coreInfoCtxKey).(data.Info).Info()
    if err != nil {
        //TODO handle error
        panic(err)
    }
    return info
}

func CtxHorizon(h *horizon.Connector) func(context.Context) context.Context {
    return func(ctx context.Context) context.Context {
        return context.WithValue(ctx, horizonCtxKey, h)
    }
}

func Horizon(r *http.Request) *horizon.Connector {
    return r.Context().Value(horizonCtxKey).(*horizon.Connector)
}


func CtxAccountQ(q *horizon2.AccountQ) func(context.Context) context.Context {
    return func(ctx context.Context) context.Context {
        return context.WithValue(ctx, accountQCtxKey, q)
    }
}

func AccountQ(r *http.Request) *horizon2.AccountQ {
    return r.Context().Value(accountQCtxKey).(*horizon2.AccountQ)
}

func CtxTransaction(txbuilder data.Infobuilder) func(context.Context) context.Context {
    return func(ctx context.Context) context.Context {
        ctx = context.WithValue(ctx, txBuilderCtxKey, txbuilder)
        return ctx
    }
}

func CtxAccountSignersQ(q data.AccountSigners) func(context.Context) context.Context {
    return func(ctx context.Context) context.Context {
        return context.WithValue(ctx, accountSignersCtxKey, q)
    }
}

func AccountSignersQ(r *http.Request) data.AccountSigners {
    return r.Context().Value(accountSignersCtxKey).(data.AccountSigners).New()
}

func CtxAcc(q data.Accs) func(context.Context) context.Context {
    return func(ctx context.Context) context.Context {
        return context.WithValue(ctx, accCtxKey, q)
    }
}

func AccQ(r *http.Request) data.Accs {
    return r.Context().Value(accCtxKey).(data.Accs).New()
}






