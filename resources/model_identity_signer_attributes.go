/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type IdentitySignerAttributes struct {
	Identity int32 `json:"identity"`
	RoleId   int32 `json:"role_id"`
	Weight   int32 `json:"weight"`
}
