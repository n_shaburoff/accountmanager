/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type IdentitySigner struct {
	Key
	Attributes IdentitySignerAttributes `json:"attributes"`
}
type IdentitySignerResponse struct {
	Data     IdentitySigner `json:"data"`
	Included Included       `json:"included"`
}

type IdentitySignerListResponse struct {
	Data     []IdentitySigner `json:"data"`
	Included Included         `json:"included"`
	Links    *Links           `json:"links"`
}

// MustIdentitySigner - returns IdentitySigner from include collection.
// if entry with specified key does not exist - returns nil
// if entry with specified key exists but type or ID mismatches - panics
func (c *Included) MustIdentitySigner(key Key) *IdentitySigner {
	var identitySigner IdentitySigner
	if c.tryFindEntry(key, &identitySigner) {
		return &identitySigner
	}
	return nil
}
